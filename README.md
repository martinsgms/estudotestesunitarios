# Estudo de Testes Unitários

Projeto adicionado ao GitLab para estudar os recursos de CI/CD
Repositório original no GitHub

Se trata de um sistema simples de locadora de filmes, onde o enfoque é a aplicação de testes automatizados e suas técnicas.
Projeto que pertence ao curso de Wagner Costa, na Udemy.

Utiliza:
- Java;
- JUnit;
- Mockito;
- Power Mock;
- lib Build Master;
- TDD;
- DDT;
- Maven; 
- Paralelismo de testes.
 